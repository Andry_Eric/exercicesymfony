<?php
/**
 * Created by PhpStorm.
 * User: Allur-11
 * Date: 26/11/2019
 * Time: 15:07
 */

namespace App\Manager;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Routing\Annotation\Route;


class UserManager extends AbstractController implements UserManagerInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
   {

       $this->container = $container;
       $this->entityManager = $this->getDoctrine()->getManager();
       
   }

    public function getEmCustormer($login)
    {
        $conn = $this->customerConnection()->getConnection();
        $sql = "SELECT * FROM user WHERE login = :login";
        $query = $conn->prepare($sql);
        $query->execute(array('login' => $login));
        $userCustomer = $query->fetchAll();
        $user1 = new User();
        $dbname = $conn->getParams()['dbname'];
        if ($userCustomer){
            $id = (int) $userCustomer[0]['id'];
            $user1->setId($id);
            $user1->setLogin($userCustomer[0]['login']);
            $user1->setMdp($userCustomer[0]['mdp']);
           return $user1;
        }
        // throw new CustomUserMessageAuthenticationException('Email could not be found.');
        return $this->redirectToRoute('app_login',['bdd' => $dbname]);

    }
    public function convertToObject($array) {
        $object = new stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = convertToObject($value);
            }
        $object->$key = $value;
        }
        return $object;
    }

    public function customerConnection()
    {
        $bdd = '';
        if (array_key_exists('REQUEST_URI', $_SERVER)) {
            $bdd = str_replace('/login', '', $_SERVER['REQUEST_URI']);
        }
        $bdd = str_replace('/', '', $bdd);
        $conn = array(
            'driver'   => 'pdo_mysql',
            'user'     => 'root',
            'password' => '',
            'dbname'   => $bdd
        );
        $customer = \Doctrine\ORM\EntityManager::create(
            $conn,
            $this->entityManager->getConfiguration(),
            $this->entityManager->getEventManager()
        );
        return $customer;

    }
}