<?php

namespace App\Security;

use App\Entity\User;
use App\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Routing\Annotation\Route;



class LoginCustomAuthenticator  extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;


    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;
    private $container;
    private $userManager;

    public function __construct(EntityManagerInterface $entityManager,
                                UrlGeneratorInterface $urlGenerator,
                                CsrfTokenManagerInterface $csrfTokenManager,
                                UserPasswordEncoderInterface $passwordEncoder,
                                ContainerInterface $container, UserManager $userManager)
    {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->container = $container;
        $this->emC = $this->container->get('doctrine.orm.customer_entity_manager');
        $this->userManager = $userManager;
    }

    public function getDoctrine()
    {
        return $this->entityManager;
    }
    public function supports(Request $request){

        return 'app_login' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {

        $credentials = [
            'login' => $request->request->get('email'),
            'mdp' => $request->request->get('pass'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['login']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {

        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $connexion = $this->userManager->customerConnection()->getConnection();
        
        $sql = "SELECT * FROM user WHERE login = :login";
        $test = $connexion->prepare($sql);
        $test->execute(array('login' => $credentials['login']));
        $userCustomer = $test->fetchAll();
        $User = new User();
        $dbname = $connexion->getParams()['dbname'];
        
        if (!$userCustomer){
            // return $this->redirectToRoute('app_login', ['bdd' => $dbname]);
            throw new CustomUserMessageAuthenticationException('L\'identifiant n\'existe pas dans '.$dbname. ' .');
        }
        
        $id = (int) $userCustomer[0]['id'];
        $User->setId($id);
        $User->setIdClient($userCustomer[0]['id_client']);
        $User->setIdUser($userCustomer[0]['id_user']);
        $User->setLogin($userCustomer[0]['login']);
        $User->setPassword($userCustomer[0]['mdp']);

        return $User;
    }


    public function checkCredentials($credentials, UserInterface $user)
    {   
        $checkUser = $this->passwordEncoder->isPasswordValid($user, $credentials['mdp']);
        if (!$checkUser){
            throw new CustomUserMessageAuthenticationException('L\'identifiant n\'existe pas.');
        }
        return $checkUser;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

    }

    protected function getLoginUrl()
    {
        $dbname = $this->userManager->customerConnection()->getConnection()->getParams()['dbname'];
        return $this->urlGenerator->generate('app_login', ['bdd' => $dbname]);
    }
}
