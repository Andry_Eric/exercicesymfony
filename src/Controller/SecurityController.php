<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Manager\UserManager;
use Symfony\Component\HttpFoundation\Request;


class SecurityController extends AbstractController
{

     public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
        $this->bdd = $this->userManager->customerConnection()->getConnection()->getParams()['dbname'];

    }
    /**
     * @Route("/{bdd}", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        
        if ($this->getUser()) {
            return $this->forward('App\Controller\UserController::index', ['last_username' => $authenticationUtils->getLastUsername()]);
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        // get bdd name
        $db_name = $this->bdd;

        return $this->render('security/index.html.twig', ['last_username' => $lastUsername, 'error' => $error, 'db_name' => $db_name]);
    }

      /**
     * @Route("/", name="home")
     */
    public function home()
    {
        return $this->render('security/home.html.twig');
    }

    /**
     * @Route("/{bdd}/logout", name="app_logout")
     */
    public function logout(Request $request)
    {
        // throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
        return $this->redirectToRoute('app_login', ['bdd' => $this->bdd]);
    }
}
