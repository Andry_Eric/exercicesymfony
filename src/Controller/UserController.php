<?php

namespace App\Controller;

use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Manager\UserManager;

class UserController extends AbstractController
{

    protected $container;

    public function __construct(UserManager $userManager)
    {

        $this->userManager = $userManager;
        $this->bdd = $this->userManager->customerConnection()->getConnection()->getParams()['dbname'];
    }

    /**
     * @Route("/{bdd_user}", name="user")
     */
    public function index(EntityManagerInterface $entityManager)
    {

        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login', ['bdd' => $this->bdd]);
        }
        // Get all users bdd1
        $getconnexion = $this->userManager->customerConnection()->getConnection();
        $sql = "SELECT * FROM user";
        $qb = $getconnexion->prepare($sql);
        $qb->execute();
        $users = $qb->fetchAll();
        $idClient = $this->getUser()->getIdClient();
        // Get all bdd2
        $db_client = $this->getDoctrine()->getManager()->getConnection()->getParams()['dbname'];
        $clients = $this->getDoctrine()
            ->getRepository(Client::class)
            ->findById($idClient);
        return $this->render('user/index.html.twig', [
            'users' => $users,
            'clients' => $clients[0],
            'db_name' => $this->bdd,
            'db_client' => $db_client
        ]);
    }

}
