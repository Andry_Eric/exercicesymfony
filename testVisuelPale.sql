-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour bdd1
CREATE DATABASE IF NOT EXISTS `bdd1` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bdd1`;

-- Listage de la structure de la table bdd1. client
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C7440455E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bdd1.client : ~2 rows (environ)
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
REPLACE INTO `client` (`id`, `email`, `roles`, `password`) VALUES
	(1, 'client@gmail.com', '["ROLE_ADMIN"]', '$2y$12$OB79pmZI5MMsJQgvmhnuLOodB13E/pnvuKRkpYXCQqPa7QtfN/Z.u'),
	(2, 'client2@gmail.com', '["ROLE_ADMIN"]', '$2y$12$OB79pmZI5MMsJQgvmhnuLOodB13E/pnvuKRkpYXCQqPa7QtfN/Z.u');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;

-- Listage de la structure de la table bdd1. migration_versions
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bdd1.migration_versions : ~1 rows (environ)
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
REPLACE INTO `migration_versions` (`version`, `executed_at`) VALUES
	('20110930234040', '2011-09-30 23:42:28');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;

-- Listage de la structure de la table bdd1. user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bdd1.user : ~1 rows (environ)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `email`, `roles`, `password`) VALUES
	(1, 'userbdd1@mail.com', '["ROLE_CLIEN"]', '$2y$12$OB79pmZI5MMsJQgvmhnuLOodB13E/pnvuKRkpYXCQqPa7QtfN/Z.u');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Listage de la structure de la base pour bdd2
CREATE DATABASE IF NOT EXISTS `bdd2` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bdd2`;

-- Listage de la structure de la table bdd2. client
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C7440455E7927C74` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bdd2.client : ~0 rows (environ)
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
/*!40000 ALTER TABLE `client` ENABLE KEYS */;

-- Listage de la structure de la table bdd2. migration_versions
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bdd2.migration_versions : ~1 rows (environ)
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
REPLACE INTO `migration_versions` (`version`, `executed_at`) VALUES
	('20110930234040', '2011-09-30 23:41:37');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;

-- Listage de la structure de la table bdd2. user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bdd2.user : ~2 rows (environ)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `email`, `roles`, `password`) VALUES
	(1, 'user@gmail.com', '["ROLE_ADMIN"]', '$2y$12$OB79pmZI5MMsJQgvmhnuLOodB13E/pnvuKRkpYXCQqPa7QtfN/Z.u'),
	(2, 'user2@gmail.com', '["ROLE_ADMIN"]', '$2y$12$NitQfUkexJmML3bF3/fPe.mq/eXuQ6bgIDIc2XRJQ8Op8gcTRa476');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
